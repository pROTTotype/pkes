//#include <essDisplay.h>
//#include "display.h"

//#include <Wire.h>
//#include <Flydurino.h>
//#include "fly_sensor.h"
//#include "ir_sensor.h"
//#include "motor.h"
#include "move.h"
#include <AppBuilder.h>

/*bool __attribute__((OS_buttonPress)) buttonPress(bool button) {
  bool press = 0;
  asm volatile ("LDI r16, 0x00       \n\t" 
                "CPI %1, 0       \n\t" 
                "BRNE Button1%=       \n\t"
                "SBIC  %4, %5         \n\t"
                "LDI r16, 1      \n\t" //change direction
                "MOV  %0, r16        \n\t"
                "RJMP End%=            \n\t"
                "Button1%=:           \n\t"
                "SBIC  %2, %3         \n\t"
                "LDI r16, 1      \n\t" //change counter
                "MOV  %0, r16        \n\t"
                "End%=:               \n\t"
                : "=r" (press) 
                : "w"  (button),"I" (_SFR_IO_ADDR(PING)), "I" (PING5)
                               ,"I" (_SFR_IO_ADDR(PINF)), "I" (PINF4)
                : "r16");
  return press;
}

bool mode;
FlySensorAcc *flyACC;
FlySensorRot *flyROT;
IrSensor *ir1;
IrSensor *ir2;
Motor *left;
Motor *right;*/
Move *moveobject;


AppBuilder appb(12, 5, String("AVH\0"), 2048);
int value [5] = {0,0,0,0,0};
char action [5] = {0,0,0,0,0};
int tmpvalue = 0;
int count = 0;
int startcounter = 0;
char show [25] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
char show2 [3] = {0,0,0};
boolean startchange = false;

int mydelay = 0;
uid8 la, t, disconnected = 0;

void anzeige()
{
  for(int i = 0; i<count; ++i)
  {
    show[5*i] = action[i];
    show[5*i+1] = (char)((int)(value[i]/100)+48);
    show[5*i+2] = (char)((int)((value[i]%100)/10)+48);
    show[5*i+3] = (char)((int)(value[i]%10)+48);
    show[5*i+4] = ' ';
  }
  appb.set_text(la, show);
}

void anzeige2()
{
  show2[0] = (char)((int)(tmpvalue/100)+48);
  show2[1] = (char)((int)((tmpvalue%100)/10)+48);
  show2[2] = (char)((int)(tmpvalue%10)+48);
  appb.set_text(t, show2);
}

void turn(uid8 id, char *string)
{
    if(tmpvalue != 0 && count < 5)
    {
      value[count] = tmpvalue;
      action[count] = 't';
      tmpvalue = 0;
      anzeige2();
      ++count;
      anzeige();
    }
}

void drive(uid8 id, char *string)
{
    if(/*tmpvalue != 0 && */count < 5)
    {
      value[count] = tmpvalue;
      action[count] = 'd';
      tmpvalue = 0;
      anzeige2();
      ++count;
      anzeige();
    }
}

void plusfive(uid8 id, char *string)
{
  tmpvalue += 5;
  anzeige2();
}

void plusfivezero(uid8 id, char *string)
{
  tmpvalue += 50;
  anzeige2();
}

void minusfive(uid8 id, char *string)
{
  tmpvalue -= 5;
  if(tmpvalue<0) tmpvalue = 0;
  anzeige2();
}

void minusfivezero(uid8 id, char *string)
{
  tmpvalue -= 50;
  if(tmpvalue<0) tmpvalue = 0;
  anzeige2();
}

void reset(uid8 id, char *string)
{
    for(int i = 0; i<5; ++i)
    {
      value [i] = 0;
      action [i] = 0;
    }
    for(int i = 0; i<25; ++i)
    {
      show[i] = 0;
    }
    tmpvalue = 0;
    anzeige2();
    count = 0;
    anzeige();
}

void start(uid8 id, char *string)
{
    startchange = true;
}

void serialEvent()
{
    appb.serial_event();
}

void onconnect (uid8 id, char *unwichtig)
{

    uid8 l = appb.start_layout();
    //uid8 b4 = appb.add_button();
    uid8 b5 = appb.add_button();
    //uid8 b6 = appb.add_button();
    uid8 b7 = appb.add_button();
    t = appb.add_label();
    la = appb.add_label();
    uid8 b0 = appb.add_button();
    uid8 b1 = appb.add_button();
    uid8 b2 = appb.add_button();
    uid8 b3 = appb.add_button();
    appb.end_layout(l);

    appb.send_components();
    //appb.sendappb();
    //Serial.println("Hallo Welt");
    appb.set_text(b0, "Drehen");
    appb.set_text(b1, "Fahren");
    appb.set_text(b2, "Abbrechen");
    appb.set_text(b3, "Start");
    //appb.set_text(b4, "-5");
    appb.set_text(b5, "+5");
    //appb.set_text(b6, "-50");
    appb.set_text(b7, "+50");

    appb.add_callback(b0, turn);
    appb.add_callback(b1, drive);
    appb.add_callback(b2, reset);
    appb.add_callback(b3, start);
    
    //appb.add_callback(b4, minusfive);
    appb.add_callback(b5, plusfive);
    //appb.add_callback(b6, minusfivezero);
    appb.add_callback(b7, plusfivezero);

    digitalWrite(13, LOW);
}

void ondisconnect (uid8 id, char *unwichtig)
{
    //mydelay = 1000;
    digitalWrite(13, HIGH);
    disconnected = 1;
}




void setup() {
  /*float version=0.4;
  
  Serial.begin(9600);
  Serial.println("----------------------------------");
  Serial.println("PKES Wintersemester 2014/15"       );
  Serial.print  ("Vorlage 3. essBot - Version "      );
  Serial.println(version                             );
  Serial.println("----------------------------------");

  initDisplay();
  
  flyACC = new FlySensorAcc();
  flyROT = new FlySensorRot();
  ir1    = new IrSensor(2);
  ir2    = new IrSensor(3);*/
  moveobject = new Move();
  
  /*mode = 1;
  
  pinMode(3, OUTPUT);
  pinMode(11, OUTPUT);
  TCCR1A = _BV(WGM11) | _BV(WGM10) | _BV(COM1A1) | _BV(COM1B1) | _BV(COM1C1);
  TCCR1B = _BV(CS11);
  TCCR3A = _BV(WGM31) | _BV(WGM30) | _BV(COM3A1) | _BV(COM3B1) | _BV(COM3C1);
  TCCR3B = _BV(CS31);
  pinMode(12, INPUT);
  pinMode(13, INPUT);
 
  EICRB = _BV(ISC40);
  EIMSK = _BV(INT4);
  PCICR = _BV(PCIE0);
  PCMSK0 = _BV(PCINT4);
  sei();
  //cli();

  right = new Motor(&OCR3CH, &OCR3CL, &PORTB, 6, -1);
  left  = new Motor(&OCR1AH, &OCR1AL, &PORTB, 7, 1);*/
  
  // initialize digital pin 13 as an output.
    // Serial.println("INITIALIZING");
    pinMode(13, OUTPUT);

    digitalWrite(13, HIGH);

    appb.set_onconnect(onconnect);
    appb.set_ondisconnect(ondisconnect);

    Serial.begin(9600);
  
}

/*float U = 5*3.14; //?
int A = 120;

ISR(INT4_vect)
{
  moveobject->changeDistanceleft(-(U/(A*2)));
};
  
ISR(PCINT0_vect)
{
  moveobject->changeDistanceright(-(U/(A*2)));
};

float targetRotation;
int movementPowerLeft = -710;
int movementPowerRight = -680;

void moveForward()
{
  left->move(-movementPowerLeft);
  right->move(movementPowerRight);
}

void moveBackward()
{
  left->move(movementPowerLeft);
  right->move(-movementPowerRight);
}
  
void rotateLeft()
{
  left->move(movementPowerLeft);
  right->move(movementPowerRight);
}
  
void rotateRight()
{
  left->move(-movementPowerLeft);
  right->move(-movementPowerRight);
}

void stopMovement()
{
  left->move(0);
  right->move(0);
}

int stage = 0;
int timer = 0;*/

void loop()
{
  appb.refresh();

    if (!disconnected)
    {
        if(startchange)
        {
          if(!moveobject->driveDone())
          {
            moveobject->update();
          }
          else if (startcounter < count)
          {
            if(action[startcounter] == 'd') moveobject->drive(value[startcounter]);
            else moveobject->rotate(value[startcounter]); 
            ++startcounter; 
          }
          else
          {
            startcounter = 0;
            for(int i = 0; i<5; ++i)
            {
              value [i] = 0;
              action [i] = 0;
            }
            for(int i = 0; i<25; ++i)
            {
              show[i] = 0;
            }
            tmpvalue = 0;
            anzeige2();
            count = 0;
            anzeige();
            startchange = false;
          }
        }
    }
    else
    {
        digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
        delay(mydelay);              // wait for a second
        digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
        delay(mydelay);              // wait for a second
    }
}
