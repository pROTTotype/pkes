#include <Wire.h>
#include <Flydurino.h>

/*          |   ACCELEROMETER    |           GYROSCOPE
 * DLPF_CFG | Bandwidth | Delay  | Bandwidth | Delay  | Sample Rate
 * ---------+-----------+--------+-----------+--------+-------------
 * 0        | 260Hz     | 0ms    | 256Hz     | 0.98ms | 8kHz
 * 1        | 184Hz     | 2.0ms  | 188Hz     | 1.9ms  | 1kHz
 * 2        | 94Hz      | 3.0ms  | 98Hz      | 2.8ms  | 1kHz
 * 3        | 44Hz      | 4.9ms  | 42Hz      | 4.8ms  | 1kHz
 * 4        | 21Hz      | 8.5ms  | 20Hz      | 8.3ms  | 1kHz
 * 5        | 10Hz      | 13.8ms | 10Hz      | 13.4ms | 1kHz
 * 6        | 5Hz       | 19.0ms | 5Hz       | 18.6ms | 1kHz
 * 7        |   -- Reserved --   |   -- Reserved --   | Reserved
 */

Flydurino *flydurino;

int16_t rot_x, rot_y, rot_z;

void setup() {
  flydurino = new Flydurino();

  // try other modes ... see the table
  flydurino->setDLPFMode(0);

  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  flydurino->getRotation( &rot_x, &rot_y, &rot_z);
  
  Serial.print("rot_x: ");    Serial.print(rot_x); 
  Serial.print("\t rot_y: "); Serial.print(rot_y);
  Serial.print("\t rot_z: "); Serial.println(rot_z);
}