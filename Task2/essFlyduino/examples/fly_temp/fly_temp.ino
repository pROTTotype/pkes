#include <Wire.h>
#include <Flydurino.h>

Flydurino *flydurino;

double temp;

void setup() {
  flydurino = new Flydurino();
  Serial.begin(9600);
}

void loop() {
  flydurino->getTemperature( &temp);
  
  Serial.print("temp: "); Serial.println(temp);
}