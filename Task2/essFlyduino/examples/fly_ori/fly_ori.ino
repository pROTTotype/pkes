#include <Wire.h>
#include <Flydurino.h>

Flydurino *flydurino;

int16_t ori_x, ori_y, ori_z;

void setup() {
  flydurino = new Flydurino();
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  flydurino->getOrientation( &ori_x, &ori_y, &ori_z);
  
  Serial.print("ori_x: ");    Serial.print(ori_x); 
  Serial.print("\t ori_y: "); Serial.print(ori_y);
  Serial.print("\t ori_z: "); Serial.println(ori_z);
}