#include <avr/io.h>
#include <essDisplay.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef DISPLAY_H
#define DISPLAY_H

void writetoDisplay(int value);

void writetoDisplay(float value, char dec=1);

/**
 * @brief Realization of a spirit level (according to the example video)
 *
 *
 * As a simple five-finger exercise ... you can choose your parameters freely ;)
 * 
 * @param x orientation
 * @param y orientation
 */
 
int zahlzubin(int i);

void errorcode();

int first_position (int value);

int second_position (int value);

int third_position (int value);

void nodecimal (int first_position, int second_position, int third_position, char dec);

void writetoDisplay3int(int first_position, int second_position, int third_position);

void balanceDisplay(double x, double y);

char convertdouble(double x, double y, int counter);

#endif


