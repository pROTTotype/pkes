#include <essDisplay.h>
#include "display.h"

#include <Wire.h>
#include <Flydurino.h>
#include "fly_sensor.h"
#include "ir_sensor.h"

bool __attribute__((OS_buttonPress)) buttonPress(bool button) {
  // replace with your solution
  bool press = 0;
  asm volatile (  "LDI r16, 0x00   \n\t"
                "CPI %1, 0       \n\t"
                "BRNE Button1%=  \n\t"
                "SBIC %4, %5     \n\t"
                "LDI r16, 1      \n\t" 
                "MOV %0, r16     \n\t"
                "RJMP End%=      \n\t"
                "Button1%=:      \n\t"
                "SBIC %2, %3     \n\t"
                "LDI r16, 1      \n\t" 
                "MOV %0, r16     \n\t"
                "End%=:          \n\t"
              : "=r" (press)
              : "w" (button),"I" (_SFR_IO_ADDR(PING)), "I" (PING5)
                            ,"I" (_SFR_IO_ADDR(PINF)), "I" (PINF4)
              : "r16");
  return press;
}

bool mode;
FlySensorAcc *flyACC;
IrSensor *ir;

void setup() {
  float version = 0.4;
    
  Serial.begin(9600);
  Serial.println("----------------------------------");
  Serial.println("PKES Wintersemester 2014/15"       );
  Serial.print  ("Vorlage 2. essBot - Version "      );
  Serial.println(version                             );
  Serial.println("----------------------------------");

  initDisplay();
  
  flyACC = new FlySensorAcc();
  ir     = new IrSensor(3);
  mode = 0;
}

void loop() {
  delay(200);
  if(buttonPress(0)){
    mode = !mode;
  }
  
  if(mode){
    float acc[3];
    flyACC->getMeasurement(acc);
    Serial.print("Orientation; "); Serial.print(acc[0]); Serial.print(", "); Serial.print(acc[1]); Serial.print(", "); Serial.println(acc[2]);
    balanceDisplay(acc[0], acc[1]);
  } else {
    uint8_t distance;
    ir->getMeasurement(&distance);
    Serial.print("Distance IR: "); Serial.println(distance);
    writetoDisplay((int)distance);
  }  
}


