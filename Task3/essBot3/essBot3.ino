#include <essDisplay.h>
#include "display.h"

#include <Wire.h>
#include <Flydurino.h>
#include "fly_sensor.h"
#include "ir_sensor.h"
#include "motor.h"

bool __attribute__((OS_buttonPress)) buttonPress(bool button) {
  // replace with your solution
  bool press = 0;
  asm volatile (  "LDI r16, 0x00   \n\t"
                "CPI %1, 0       \n\t"
                "BRNE Button1%=  \n\t"
                "SBIC %4, %5     \n\t"
                "LDI r16, 1      \n\t" 
                "MOV %0, r16     \n\t"
                "RJMP End%=      \n\t"
                "Button1%=:      \n\t"
                "SBIC %2, %3     \n\t"
                "LDI r16, 1      \n\t" 
                "MOV %0, r16     \n\t"
                "End%=:          \n\t"
              : "=r" (press)
              : "w" (button),"I" (_SFR_IO_ADDR(PING)), "I" (PING5)
                            ,"I" (_SFR_IO_ADDR(PINF)), "I" (PINF4)
              : "r16");
  return press;
}

bool mode;
bool stop;
FlySensorAcc *flyACC;
FlySensorRot *flyROT;
IrSensor *ir1;
IrSensor *ir2;
Motor *left;
Motor *right;

void setup() {
  float version=0.4;
  
  Serial.begin(9600);
  Serial.println("----------------------------------");
  Serial.println("PKES Wintersemester 2014/15"       );
  Serial.print  ("Vorlage 3. essBot - Version "      );
  Serial.println(version                             );
  Serial.println("----------------------------------");

  initDisplay();
  
  flyACC = new FlySensorAcc();
  flyROT = new FlySensorRot();
  ir1    = new IrSensor(2);
  ir2    = new IrSensor(3);
  
  mode = 0;
  stop = 1;
  
  // TODO: 
  // 1. pwm-config for OC1A and OC3C
  //    use phase-correct pwm with 10bits, prescaler is optional
  // 2. set the data-direction for PWM-channels
  //    arduino digital pins 11 and 3 
  // 3. set the data-direction for motor direction pins
  //    located at arduino digital pin 12 and 13
 
pinMode(3, OUTPUT);
pinMode(11, OUTPUT);
TCCR1A = _BV(WGM11) | _BV(WGM10) | _BV(COM1A1) | _BV(COM1B1) | _BV(COM1C1);
TCCR1B = _BV(CS11);
TCCR3A = _BV(WGM31) | _BV(WGM30) | _BV(COM3A1) | _BV(COM3B1) | _BV(COM3C1);
TCCR3B = _BV(CS31);
pinMode(12, INPUT);
pinMode(13, INPUT);
/*
TCCR1A = (1 << WGM11) | (1 << WGM10) | (1 << COM1A1);
TCCR1B = (1 << CS12) | (1 << CS10);

TCCR3A = (1 << WGM31) | (1 << WGM30) | (1 << COM3C1);
TCCR3B = (1 << CS32)| (1 << CS30);
DDRE = (1 << PORTE5); //Pin 3 ist an PORTE
DDRB = (1 << PORTB5) | (1 << PORTB6) | (1 << PORTB7); //pins 11,12,13 an PORTB
*/

  right = new Motor(&OCR3CH, &OCR3CL, &PORTB, 6, -1);
  left  = new Motor(&OCR1AH, &OCR1AL, &PORTB, 7, 1);
  
}

int movementPowerLeft = 468; //470
int movementPowerRight = 478; //478
float targetRotation = 0;

void moveForward(){
  left->move(movementPowerLeft);
  right->move(-movementPowerRight);
  }
  
void moveBackward(){
  left->move(-movementPowerLeft);
  right->move(movementPowerRight);
  }
  
void rotateLeft(){
  left->move(-movementPowerLeft);
  right->move(-movementPowerRight);
  }
  
void rotateRight(){
  left->move(movementPowerLeft);
  right->move(movementPowerRight);
  }

void stopMovement(){
  left->move(0);
  right->move(0);
  }


void loop() {
  if(buttonPress(1)){
    stop = !stop;
  }
  if(buttonPress(0)){
    mode = !mode;
  }
float rot[3];
if(stop){
      stopMovement();
  Serial.println("STOPP"); 
    writetoDisplay(0b01101110, 0b11101110, 0b00011100);  
}else{
  
  flyROT->getMeasurement(rot);
    if(mode){
        
      writetoDisplay(rot[2]/10000, 0);
    
      //Serial.print("z-rotation:"); 
      Serial.println(rot[2]);
      //rot[2]=0;
      if (rot[2] < -30000) 
        rotateLeft();
        else if (rot[2] > 30000)
        rotateRight();
        else stopMovement();
        //moveForward();
      
    } else {
        //moveForward(); 
        int executeDistance = 25;
        writetoDisplay(2);
        uint8_t distance1;
        uint8_t tmp1;
        ir1->getMeasurement(&tmp1);
        Serial.println(tmp1);
        distance1=tmp1;  //überschreibt sonst distance1 mit 0
        uint8_t distance2;
        ir2->getMeasurement(&distance2);
        //targetRotation = 10000;
        if (rot[2] - targetRotation < -10){ 
          rotateLeft();
          Serial.println("rotateLeft()");
        }
        else if(distance1 < executeDistance && distance2 < executeDistance && rot[2] - targetRotation > -10) targetRotation += 90;
        else moveForward();
        Serial.print(distance1); Serial.print(" ; "); Serial.print(distance2); Serial.print(" ; "); Serial.print(rot[2]); Serial.print (" ; "); Serial.println(targetRotation);
    
    }
  } 
  
}
