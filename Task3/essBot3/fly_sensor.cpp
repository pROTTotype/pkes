#include "fly_sensor.h"

FlySensor::FlySensor(){
  //flydurino = new Flydurino();
  flydurino = new Flydurino(0x69);
}

/*************************************************************/

float FlySensorAcc::convertACC(int16_t v1, int16_t v2){
  //Serial.print(v1);
  //Serial.print(v2);
  //Serial.println(atan2(v1,v2));
  return atan2(v1,v2); // TODO: ggf. in grad 
}

void FlySensorAcc::getMeasurement(void *value){
  float *uiValue = (float *) value;
  
  int16_t acc_x, acc_y, acc_z;
  flydurino->getAcceleration(&acc_x, &acc_y, &acc_z);

  uiValue[0] = convertACC(acc_z, acc_x);
  uiValue[1] = convertACC(acc_z, acc_y);
  uiValue[2] = convertACC(acc_x, acc_y);
}

/*************************************************************/

FlySensorRot::FlySensorRot() : FlySensor(){  
  mRotation[0] = mRotation[1] = mRotation[2] = 0;
  configure();
  mTime = millis();  
}

/**
 * @brief offset-correction
 *
 * As many other sensors, also this gyroscope has a natural offset, in
 * dependence to the applied filter. Check the fly_rot.ino - example at:
 * File -> Examples -> essFlyduino 
 * Identify this offset and use it for further error-correction within
 * method "getMeasurement" ...
 *
 * @see http://www.invensense.com/mems/gyro/documents/PS-MPU-6000A-00v3.4.pdf
 */
void FlySensorRot::configure() {
  //flydurino->setDLPMode(6);
  mOffset[0]   = 65.0;
  mOffset[1]   = -112.0;
  mOffset[2]   = 67.0;
}

/**
 * @brief Rotation in degrees ...
 *
 * Calculate the rotation angles according to x, y, and z. Keep in mind,
 * that this gyroscope is an incremental (over time) sensor... Have also
 * a look into the datasheet for the correct convertion.
 *
 * @see http://www.invensense.com/mems/gyro/documents/PS-MPU-6000A-00v3.4.pdf
 */
 float rot_x = 0;
 float rot_y = 0;
 float rot_z = 0;
 float average = 0;
 float count = 1;
void FlySensorRot::getMeasurement(void *value){
  float *uiValue = (float *) value;
  int16_t acc_x, acc_y, acc_z;
  flydurino->getRotation(&acc_x, &acc_y, &acc_z);
  float* tmp = (float*)value;
  average = ((average * count) + (float)acc_y)/count+1;
  ++count;
   //average = ((average * count) + (float)rot_x)/(count+1);
   //++count;
  if (acc_x + mOffset[0] < -250 || acc_x + mOffset[0] > 250) rot_x += (float)acc_x + mOffset[0];
  if (acc_y + mOffset[1] < -250 || acc_y + mOffset[1] > 250) rot_y += (float)acc_y + mOffset[1];
  if (acc_z + mOffset[2] < -250 || acc_z + mOffset[2] > 250) rot_z += (float)acc_z + mOffset[2];
  uiValue[0] = rot_x/1000;
  uiValue[1] = rot_y/1000;
  uiValue[2] = rot_z/1000;
  //Serial.print("average:");Serial.println(average);
  //
  //Serial.print(rot_x); Serial.print(" ; "); Serial.print(rot_y); Serial.print(" ; "); Serial.println(rot_z);

}
