#include "ir_sensor.h"
#include <Arduino.h>

/**
 * @brief Configure the analog-channel
 *
 * The Infrared sensors are connected to (arduino) analog pin 2 and 3.
 * Use it wise during the initialization ;)
 */

IrSensor::IrSensor(uint8_t channel) {
  mChannel = channel;
  configure();
}

/**
 * @brief General Analog-Read configuration
 *
 * Configure the reference-voltage according to your SHARP-Infrared distance sensor.
 * See therefore also the specific sensor-type (marked at the top of the sensor).
 *
 * Keep in mind that the configuration is set globally.
 *
 * @see http://arduino.cc/de/Reference/AnalogReference
 */

void IrSensor::configure(){
  analogReference(DEFAULT);
}

/**
 * @brief Read in distance measurements
 *
 * Read in the analog sensor value and try to linearize the measurements accordingly.
 * Choose therefor one of the methods presented in the lecture and store the result in @p *value.
 *
 * @param *value should point to an external integer of float, see the fly_sensor example
 *
 * @see http://arduino.cc/de/Reference/AnalogRead
 */

void IrSensor::getMeasurement(void *value){
  float koof = 0.004935834; //(5 V/ 1013)
  koof = 5.0 / 1013.0;
 
  int tmp2 = 0;
  int tmp3 = 0;
  for (int i = 0; i < 15; i++){
    //delay(39);
    tmp2 += analogRead(mChannel);//variable oben beachten !
    tmp3 += analogRead(mChannel);
    //Serial.print("Channel");Serial.println(mChannel);
  }
  
  int sensorvalue1 = tmp2 / 15;
  int sensorvalue2 = tmp3 / 15;
  int average = (sensorvalue1 + sensorvalue2) / 2;
 
  //Serial.println(average);
  //Serial.println(koof);
  //Serial.println(average * koof);
  
  float foo = 28.7459596743 * pow((average * koof), -1.0998337511);
  //Serial.println(foo);
  *(int*)value = foo;
 
}



