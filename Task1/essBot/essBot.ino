#include <essDisplay.h>
#include "display.h"

/**
 * @brief Check if buttons are pressed or not...
 *
 * The functionality should be implemented in and only in inline assembly. Variable
 * @p press is used to store the return value (true if the appropriate button was
 * pressed). The input parameter @p button is only used to differentiate between
 * the two buttons on the developer shield...
 * 
 * Buttons are at (Ardino) digital pin 4 and analog pin 4, see also the PinMap con-
 * version.
 * 
 * @param button true stands for the upper button false for the other one ...
 * @return true if the button is pressed, otherwise false 
 *
 * @see loop()
 * @see http://hitboxgame.com/wordpress/wp-content/uploads/2014/05/PinMap2560sma_.png
 */
bool __attribute__((OS_buttonPress)) buttonPress(bool button) {
  bool press = 0;
  //if(analogRead(4)&&button)press = 1;
  //if(digitalRead(4)&&button==0)press = 1;
asm volatile (  "LDI r16, 0x00   \n\t"
                "CPI %1, 0       \n\t"
                "BRNE Button1%=  \n\t"
                "SBIC %4, %5     \n\t"
                "LDI r16, 1      \n\t" 
                "MOV %0, r16     \n\t"
                "RJMP End%=      \n\t"
                "Button1%=:      \n\t"
                "SBIC %2, %3     \n\t"
                "LDI r16, 1      \n\t" 
                "MOV %0, r16     \n\t"
                "End%=:          \n\t"
              : "=r" (press)
              : "w" (button),"I" (_SFR_IO_ADDR(PING)), "I" (PING5)
                            ,"I" (_SFR_IO_ADDR(PINF)), "I" (PINF4)
              : "r16");
  //asm volatile("in %0, %1" : "=r" (press) : "I" (_SFR_IO_ADDR(PORTB)), "I" (4) ); 
  /*
   asm volatile (" mov  press,PORTA          \n\t" 
                : "=r" (press) 
                : "w"  (button));
   */
  return press;
}


short int counter0 =0;
float counter1     =0;
bool  direction    =1;
bool  counter      =0;

void setup() {
  float version = 0.4;
    
  Serial.begin(9600);
  Serial.println("----------------------------------");
  Serial.println("PKES Wintersemester 2014/15"       );
  Serial.print  ("Vorlage 1. essBot - Version "      );
  Serial.println(version                             );
  Serial.println("----------------------------------");

  initDisplay();
}

/**
 * @brief Counting int and float values ...
 *
 * This loop is only used to implement the initial interface for your essBOT, the 7
 * segment display (plus one dot) and the two buttons. This is required later to
 * show the sensor measurements. Implement therefor the buttonPress() function  as
 * well as the two writetoDisplay(int) and writetoDisplay(float, char) functions,
 * which have to be implemented in display.cpp. Check if the display shows the same
 * results as received via the serial interface.
 *
 * @note http://www.atmel.com/images/doc2549.pdf
 */
void loop() {
  delay(250);
 
  if(buttonPress(0)) direction=!direction;
  
  if(buttonPress(1)) counter  =!counter;
  
  if(counter){
    counter1 += direction ? 0.1 : -0.1;
    writetoDisplay(counter1);
    Serial.print("Float: "); Serial.println(counter1);
  }
  else{
    writetoDisplay(direction ? (int)counter0++ : (int)counter0--);
    Serial.print("Int: "); Serial.println((int)counter0);
  }
}
