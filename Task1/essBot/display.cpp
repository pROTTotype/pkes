#include "display.h"
//#include <iostream>


void writetoDisplay(int value) {
  if (value < 0){
	int nvalue = value * - 1;
    	if(nvalue > 199){
		errorcode();  
	} else if (nvalue >= 100){
      		writetoDisplay(0b01100010, second_position (nvalue), third_position (nvalue));
    	} else
      		writetoDisplay(0b00000010, second_position (nvalue), third_position (nvalue));
  } else  
      	writetoDisplay(first_position (value), second_position (value), third_position (value));
  if (value > 999 || value < -199){
      errorcode();
  }
}


void writetoDisplay(float value, char dec) {
  //dec = 2;
  //value = -0.01;
  //float value2 = 99.9;  //max
  //float value3 =  -9.9; //min
  //errorcode();
  int newint;
  bool error = 0;
  switch (dec){
    case 0: { if (value < -99.0 || value > 999.0){
              error =1;
            }
		newint = (int) value;
		break;
		}
    case 1: { if (value < -9.9 || value > 99.9){
              error =1;
            }
		newint = (int) (value*10);
		break;
		} 
    case 2: { if (value < -0.99 || value > 9.99){
              error =1;
            }
		newint = (int) (value*100);
		break;
		}
  }         
  if (newint < 0){
	int nnewint =  newint* - 1;
    	if(nnewint > 199){
		errorcode();  
	} else if (nnewint >= 100){
      		nodecimal(0b01100010, second_position (nnewint), third_position (nnewint), dec);
    	} else
      		nodecimal(0b00000010, second_position (nnewint), third_position (nnewint), dec);
  } else  
      	nodecimal(first_position(newint), second_position (newint), third_position (newint), dec);
  if (newint > 999 || newint < -199 || error){
      errorcode();
  }
  
  

}

int zahlzubin(int i){
  int converted;
  
    switch (i){
    case 0: converted=0b11111100; break;
    case 1: converted=0b01100000; break;
    case 2: converted=0b11011010; break;
    case 3: converted=0b11110010; break;
    case 4: converted=0b01100110; break;
    case 5: converted=0b10110110; break;
    case 6: converted=0b10111110; break;
    case 7: converted=0b11100000; break;
    case 8: converted=0b11111110; break;
    case 9: converted=0b11100110; break;
    default: converted=0b11111111; break;
    }
    
    return converted;
}

void errorcode(){
    writetoDisplay(0b10011110, 0b00001010, 0b00001011);
}

int first_position (int value){
	return zahlzubin(value/100);
}

int second_position (int value){
	return zahlzubin(((value-((value/100)*100))/10));
}

int third_position (int value){
	return zahlzubin(value%10);
}

void nodecimal (int first_position, int second_position, int third_position, char dec){
	int i = 1;
	switch (dec){	
	case 0: writetoDisplay(first_position, second_position, third_position+i); break;
	case 1: writetoDisplay(first_position, second_position+i, third_position); break;
	case 2: writetoDisplay(first_position+i, second_position, third_position); break;
	default: writetoDisplay(0b11111111, 0b11111111, 0b11111111);
	}
}

void writetoDisplay3int(int first_position, int second_position, int third_position){
  writetoDisplay (zahlzubin(first_position), zahlzubin(second_position), zahlzubin(third_position));
}

