#include "display.h"

  char zero = 0b11111100;
  char one = 0b01100000;
  char two = 0b11011010;
  char three = 0b11110010;
  char four = 0b01100110;
  char five = 0b10110110;
  char six = 0b10111110;
  char seven = 0b11100000;
  char eight = 0b11111110;
  char nine = 0b11110110;
  char minus = 0b00000010;
  char minusone = 0b01100010;
  char nothing = 0b00000000;

void writetoDisplay(int value)
{
  int tmp = value;
  char first = nothing;
  char second = nothing;
  char third = nothing;
  int firstnumber = 0;
  int secondnumber = 0;
  int thirdnumber = 0;
  bool negative = false;
  
  if (value < 0)
  {
    negative = true;
    tmp = -tmp;
  }
  thirdnumber = tmp%10;
  tmp=tmp/10;
  if (tmp > 0)
  {
    secondnumber = tmp%10;
    tmp=tmp/10;
    if (tmp > 0)
    {
      firstnumber = tmp%10;
    }
  }
  third = switchfunc(thirdnumber); 
  if (secondnumber || firstnumber)   second = switchfunc(secondnumber);
  if (firstnumber)   first = switchfunc(firstnumber);
  if (negative)
  {
    if (!secondnumber && !firstnumber) second = minus;
    else if (!firstnumber) first = minus;
    else first = minusone;
  }
  writetoDisplay(first, second, third);
}

void writetoDisplay(float value, char dec)
{
  int tmp = (int)(value*pow(10,dec));
  char first = nothing;
  char second = nothing;
  char third = nothing;
  int firstnumber = 0;
  int secondnumber = 0;
  int thirdnumber = 0;
  bool negative = false;
  
  if (value < 0)
  {
    negative = true;
    tmp = -tmp;
  }
  thirdnumber = tmp%10;
  tmp=tmp/10;
  if (tmp > 0)
  {
    secondnumber = tmp%10;
    tmp=tmp/10;
    if (tmp > 0)
    {
      firstnumber = tmp%10;
    }
  }
  third = switchfunc(thirdnumber); 
  second = switchfunc(secondnumber);
  if (firstnumber)   first = switchfunc(firstnumber);
  if (negative)
  {
    if (!firstnumber) first = minus;
    else first = minusone;
  }
  if (dec == 1)
  {
    uint8_t tmp2 = (uint8_t)second;
    tmp2++;
    second = (char)tmp2;
  }
  else if (dec == 2)
  {
    uint8_t tmp2 = (uint8_t)first;
    tmp2++;
    first = (char)tmp2;
  }
  writetoDisplay(first, second, third);
}

char switchfunc(int value)
{
  if (value == 0) return zero;
  if (value == 1) return one;
  if (value == 2) return two;
  if (value == 3) return three;
  if (value == 4) return four;
  if (value == 5) return five;
  if (value == 6) return six;
  if (value == 7) return seven;
  if (value == 8) return eight;
  return nine;
}

int pow( int base, int expo)
{
  if(!expo) return 1;
  return base * pow(base, expo -1); 
}
  
  char top = 0b10000000;
  char mid = 0b00000010;
  char bottom = 0b00010000;
  char d_left = 0b00001100;
  char d_right = 0b01100000;
  
void balanceDisplay(double x, double y){
  char resultleft = nothing;
  char resultmid = nothing;
  char resultright = nothing;
  
  if (x > 15) resultright += d_right;
  else if (x > 10) resultright += d_left;
  else if (x > 5) resultmid += d_right;
  else if (x > -5) resultmid += d_left + d_right;
  else if (x > -10) resultmid += d_left;
  else if (x > -15) resultleft += d_right;
  else resultleft += d_left;
  
  if (y > 10)
  {
    resultleft += top;
    resultmid += top;
    resultright += top;
  }
  else if (y > -10)
  {
    resultleft += mid;
    resultmid += mid;
    resultright += mid;
  }
  else 
  {
    resultleft += bottom;
    resultmid += bottom;
    resultright += bottom;
  }
  writetoDisplay(resultleft, resultmid, resultright);
}
