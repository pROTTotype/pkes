#include <AppBuilder.h>
#include <helper.h>

#include <essDisplay.h>
#include "display.h"

#include <Wire.h>
#include <Flydurino.h>
#include "fly_sensor.h"
#include "ir_sensor.h"
#include "motor.h"
#include "move.h"

bool __attribute__((OS_buttonPress)) buttonPress(bool button) {
  bool press = 0;
  asm volatile ("LDI r16, 0x00       \n\t" 
                "CPI %1, 0       \n\t" 
                "BRNE Button1%=       \n\t"
                "SBIC  %4, %5         \n\t"
                "LDI r16, 1      \n\t" //change direction
                "MOV  %0, r16        \n\t"
                "RJMP End%=            \n\t"
                "Button1%=:           \n\t"
                "SBIC  %2, %3         \n\t"
                "LDI r16, 1      \n\t" //change counter
                "MOV  %0, r16        \n\t"
                "End%=:               \n\t"
                : "=r" (press) 
                : "w"  (button),"I" (_SFR_IO_ADDR(PING)), "I" (PING5)
                               ,"I" (_SFR_IO_ADDR(PINF)), "I" (PINF4)
                : "r16");
  return press;
}

bool mode;
FlySensorAcc *flyACC;
FlySensorRot *flyROT;
IrSensor *ir1;
IrSensor *ir2;
Motor *left;
Motor *right;
Move *moveobject;

void setup() {
  float version=0.4;
  
  Serial.begin(9600);
  Serial.println("----------------------------------");
  Serial.println("PKES Wintersemester 2014/15"       );
  Serial.print  ("Vorlage 3. essBot - Version "      );
  Serial.println(version                             );
  Serial.println("----------------------------------");

  initDisplay();
  
  flyACC = new FlySensorAcc();
  flyROT = new FlySensorRot();
  ir1    = new IrSensor(2);
  ir2    = new IrSensor(3);
  moveobject = new Move();
  
  mode = 1;
  
  pinMode(3, OUTPUT);
  pinMode(11, OUTPUT);
  TCCR1A = _BV(WGM11) | _BV(WGM10) | _BV(COM1A1) | _BV(COM1B1) | _BV(COM1C1);
  TCCR1B = _BV(CS11);
  TCCR3A = _BV(WGM31) | _BV(WGM30) | _BV(COM3A1) | _BV(COM3B1) | _BV(COM3C1);
  TCCR3B = _BV(CS31);
  pinMode(12, INPUT);
  pinMode(13, INPUT);
 
  EICRB = _BV(ISC40);
  EIMSK = _BV(INT4);
  PCICR = _BV(PCIE0);
  PCMSK0 = _BV(PCINT4);
  sei();
  //cli();

  right = new Motor(&OCR3CH, &OCR3CL, &PORTB, 6, -1);
  left  = new Motor(&OCR1AH, &OCR1AL, &PORTB, 7, 1);
}

int interruptcounter = 0;

float U = 5*3.14; //?
int A = 120;

ISR(INT4_vect)
{
  moveobject->changeDistanceleft(-(U/(A*2)));
  ++interruptcounter;
};
  
ISR(PCINT0_vect)
{
  moveobject->changeDistanceright(-(U/(A*2)));
  ++interruptcounter;
};

float targetRotation;
int movementPowerLeft = -710;
int movementPowerRight = -680;

void moveForward()
{
  left->move(-movementPowerLeft);
  right->move(movementPowerRight);
}

void moveBackward()
{
  left->move(movementPowerLeft);
  right->move(-movementPowerRight);
}
  
void rotateLeft()
{
  left->move(movementPowerLeft);
  right->move(movementPowerRight);
}
  
void rotateRight()
{
  left->move(-movementPowerLeft);
  right->move(-movementPowerRight);
}

void stopMovement()
{
  left->move(0);
  right->move(0);
}

int stage = 0;
int timer = 0;
int stoptimer = 0;

void loop()
{
  ++stoptimer;
  if (timer >= 60)
 {
   timer = 0;
   interruptcounter = 0;
 }
 if(interruptcounter > stoptimer/2) stopMovement(); // verhältnis noch berechnen
 
  ++timer;
  if (timer >= 20) timer = 20;
  if(buttonPress(0) && timer == 20)
  {
    mode = !mode;
    timer = 0;
  }
  
  float rot[3];
  flyROT->getMeasurement(rot);
  
  if(mode)
  {
    Serial.println("Stop");
    stopMovement();
    //moveForward();
  }
  else
  {
    uint8_t distance1;
    ir1->getMeasurement(&distance1);
    uint8_t distance2;
    ir2->getMeasurement(&distance2);
    switch (stage)
    {
      case 0: moveobject->drive(100);
              stage = 1;
              break;
      case 1: moveobject->update();
              if (moveobject->driveDone()) stage = 2;
              Serial.println("case 1");
              break;
      case 2: moveobject->rotate(180);
              stage = 3;
              break;
      case 3: moveobject->update();
              if (moveobject->driveDone()) stage = 4;
              Serial.println("case 3");
              break;
      case 4: moveobject->drive(100);
              stage = 5;
              break;
      case 5: moveobject->update();
              if (moveobject->driveDone()) stage = 6;
              Serial.println("case 5");
              break;
      case 6: moveobject->rotate(180);
              stage = 7;
              break;
      case 7: moveobject->update();
              if (moveobject->driveDone()) stage = 8;
              Serial.println("case 7");
              break;
      case 8: stopMovement();
              break;
              
    
    }
  }
}
