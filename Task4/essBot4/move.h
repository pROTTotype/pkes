#include <arduino.h>
#include <avr/interrupt.h>

class Move
{
public:

  Move()
  {
    right = new Motor(&OCR3CH, &OCR3CL, &PORTB, 6, -1);
    left  = new Motor(&OCR1AH, &OCR1AL, &PORTB, 7, 1);
    distanceleft = 0;
    distanceright = 0;
    PowerLeft = -710;
    PowerRight = -680;
    forward = true;
    turn = true;   //ture left; false right
  };
  
  void drive(float distance)
  {
    distanceleft = distance;
    distanceright = distance;
    forward = true;
    Serial.print(distanceleft);Serial.print(" : ");Serial.println(distanceright);
  };
  
  void rotate(float angle)
  {
    if(angle < 0){
        angle = -angle; 
        turn = false;   
    }else turn = true;
    distanceleft = (angle/240)*3.14*5;
    distanceright = (angle/240)*3.14*5;
    forward = false;
  };
  
  void changeDistanceright(float value)
  {
    distanceright += value;
  }
  
  void changeDistanceleft(float value)
  {
    distanceleft += value;
  }
  
  boolean driveDone()
  {
    if (distanceright <= 0 && distanceleft <= 0) return true;
    else return false;
  }
  
  void update()
  {
    if(forward) //movment forward
    {
      if(distanceleft > 0 && distanceleft - distanceright > -1){Serial.print(distanceleft);Serial.print(" : "); left->move(-PowerLeft);}
      else left->move(0);
      if(distanceright > 0 && distanceright - distanceleft > -1){Serial.println(distanceright); right->move(PowerRight);}
      else right->move(PowerRight-10);
    }
    else // rotation
    {
      if(turn){ 
      if(distanceleft > 0 && distanceleft - distanceright > -1) left->move(PowerLeft);
      else left->move(0);
      if(distanceright > 0 && distanceright - distanceleft > -1) right->move(PowerRight);
      else right->move(0);
      }else{
      if(distanceleft > 0 && distanceleft - distanceright > -1) left->move(-PowerLeft);
      else left->move(0);
      if(distanceright > 0 && distanceright - distanceleft > -1) right->move(-PowerRight);
      else right->move(0);
      } 
    }
  };
  
private:
  Motor* left;
  Motor* right;
  float distanceleft;
  float distanceright;
  int PowerLeft;
  int PowerRight;
  boolean forward;
  boolean turn;
};

