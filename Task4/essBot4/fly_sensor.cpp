#include "fly_sensor.h"

FlySensor::FlySensor(){
  flydurino = new Flydurino();
}

/*************************************************************/

float FlySensorAcc::convertACC(int16_t v1, int16_t v2){
  return atan((float)v2/(float)v1)*180/3.14;
}

void FlySensorAcc::getMeasurement(void *value){
  float *uiValue = (float *) value;
  
  int16_t acc_x, acc_y, acc_z;
  flydurino->getAcceleration(&acc_x, &acc_y, &acc_z);

  uiValue[0] = convertACC(acc_z, acc_x);
  uiValue[1] = convertACC(acc_z, acc_y);
  uiValue[2] = convertACC(acc_x, acc_y);
}

/*************************************************************/

FlySensorRot::FlySensorRot() : FlySensor(){  
  mRotation[0] = mRotation[1] = mRotation[2] = 0;
  configure();
  mTime = millis();  
}

/**
 * @brief offset-correction
 *
 * As many other sensors, also this gyroscope has a natural offset, in
 * dependence to the applied filter. Check the fly_rot.ino - example at:
 * File -> Examples -> essFlyduino 
 * Identify this offset and use it for further error-correction within
 * method "getMeasurement" ...
 *
 * @see http://www.invensense.com/mems/gyro/documents/PS-MPU-6000A-00v3.4.pdf
 */
void FlySensorRot::configure() {
  flydurino->setDLPFMode(6);
  mOffset[0]   = 65.0;
  mOffset[1]   = -112.0;
  mOffset[2]   = 67.0;
}

/**
 * @brief Rotation in degrees ...
 *
 * Calculate the rotation angles according to x, y, and z. Keep in mind,
 * that this gyroscope is an incremental (over time) sensor... Have also
 * a look into the datasheet for the correct convertion.
 *
 * @see http://www.invensense.com/mems/gyro/documents/PS-MPU-6000A-00v3.4.pdf
 */
 
 
 float g_rot_x = 0;
 float g_rot_y = 0;
 float g_rot_z = 0;
 //float average = 0;
 //int count = 0;
void FlySensorRot::getMeasurement(void *value){
  int16_t rot_x, rot_y, rot_z;
  flydurino->getRotation( &rot_x, &rot_y, &rot_z);
  float* tmp = (float*)value;
   //average = ((average * count) + (float)rot_x)/(count+1);
   //++count;
   if (rot_x + mOffset[0] < -250 || rot_x + mOffset[0] > 250) g_rot_x += (float)rot_x + mOffset[0];
   if (rot_y + mOffset[1] < -250 || rot_y + mOffset[1] > 250) g_rot_y += (float)rot_y + mOffset[1];
   if (rot_z + mOffset[2] < -250 || rot_z + mOffset[2] > 250) g_rot_z += (float)rot_z + mOffset[2];
   tmp[0] = g_rot_x;
   tmp[1] = g_rot_y;
   tmp[2] = g_rot_z;
   //Serial.print("average:"); Serial.println(average);
   //Serial.print(g_rot_x); Serial.print(" : "); Serial.print(g_rot_y); Serial.print(" : "); Serial.println(g_rot_z);
 }
